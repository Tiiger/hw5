import java.util.*;

public class Tnode {

	private String name;
	private Tnode firstChild;
	private Tnode nextSibling;

	public Tnode() 
	{
	}

	public Tnode(String s) 
	{
		name = s;
	}

	@Override
	public String toString() 
	{
		StringBuffer b = new StringBuffer();
		b.append(this.name);
		if (this.firstChild != null) 
		{
			b.append("(");
			b.append(this.firstChild.toString());
		}
		if (this.nextSibling != null) 
		{
			b.append(",");
			b.append(this.nextSibling.toString());
			b.append(")");
		}
		return b.toString();
	}

	public static Tnode buildFromRPN(String pol) 
	{
		Tnode root = null;
		StringTokenizer st = new StringTokenizer(pol, " ");
		Stack<Tnode> stack = new Stack<Tnode>();
		Tnode t1, t2;
		int i = 1;
		int counter = st.countTokens();
		while (st.hasMoreElements()) 
		{
			String s = (String) st.nextElement();
			if (s.equals("-") || s.equals("+") || s.equals("/") || s.equals("*")) 
			{
					root = new Tnode(s);
					t1 = stack.pop();
					t2 = stack.pop();
					root.nextSibling = t1;
					root.firstChild = t2;
					stack.push(root);
			} 
			else 
			{
				if (i == counter && i > 1) 
				{
					throw new RuntimeException("Expression must be ended with an operator");
				}
				else if (s.matches("[+-]?(\\d*\\.?\\d+)|(\\d+\\.?\\d*)")) 
				{
					Tnode node = new Tnode(s);
					stack.push(node);
				} 
				else 
				{
					throw new RuntimeException("Unknown operator");
				}
			}
			i++;
		}
		root = stack.pop();
		return root;
	}

	public static void main(String[] param) {
		String rpn = "2 1 - 4 * 6 3 / +";
		System.out.println("RPN 1: " + rpn);
		Tnode res = buildFromRPN(rpn);
		System.out.println("Tree 1: " + res);

	}
}


/*VIITED:
 * http://enos.itcollege.ee/~jpoial/algoritmid/puud.html
 * http://enos.itcollege.ee/~jpoial/algoritmid/adt.html
 * http://stackoverflow.com/questions/21716830/java-using-a-stack-to-reverse-the-words-in-a-sentence
 * http://stackoverflow.com/questions/19895308/converting-from-reverse-polish-notationrpn-into-a-tree-form
 * http://www.drdobbs.com/database/a-generic-iterator-for-tree-traversal/184404325
 * https://docs.oracle.com/javase/7/docs/api/java/util/StringTokenizer.html
 * https://www.cs.colostate.edu/~cs165/.Spring17/assignments/P7/files/AbstractTree.java
 * kaasõpilaste abi
 */
